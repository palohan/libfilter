// libfilter.test.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <map>
#include <cstring>

#ifdef USE_LIBFILTER
#include "../../libfilterProj/stdafx.h"
#endif // USE_LIBFILTER

#include "../libsrc/WaveFilter.h"

using namespace std;
using namespace std::filesystem;


std::map<int, pair<int, int(*)()>> testResults;

enum { mono, stereo };
const char* inputs[] = { "samples/rjs01.wav", "samples/rjsStereo.wav" };


#define testMethod(num) int testMethod##num() { const char* outputs[] = { "results/monoResult" #num ".wav", "results/stereoResult" #num ".wav" }; 

#define runTest(num)	testResults[num] = { -1, testMethod##num }; evalTest(num);


void evalTest(int testNo)
{
	cout << endl << "running test " << testNo << endl;

	try
	{
		testResults[testNo].first = testResults[testNo].second();
	}
	catch (exception& ex) 
	{
		cerr << "test #" << testNo << ": " << ex.what() << endl;
	}
}

void logTests()
{
	cout << endl;

	for (auto& res : testResults)
		cout << "test #" << res.first << ": exit code=" << res.second.first << endl;
}


testMethod(1)
	WaveFilter vaweFilter;

	vaweFilter.loadInputFile(inputs[mono]);
	vaweFilter.setParams(true, 431.37, 0.995);
	vaweFilter.filter();

	return vaweFilter.saveOutput(outputs[mono]);
}

testMethod(2)
	WaveFilter vaweFilter;

	vaweFilter.loadInputFile(inputs[mono]);
	vaweFilter.setCoeffsFile("logs/IIR_431.37_8000_1.txt");

	vaweFilter.filter();

	return vaweFilter.saveOutput(outputs[mono]);
}

testMethod(3)
	const char* args[] = { "prog", "-i", inputs[mono], "-C", "logs/IIR_431.37_8000_1.txt", "-o", outputs[mono] };

	WaveFilter vaweFilter;
	return vaweFilter.runFromArgs(sizeof(args) / sizeof(char*), args);
}

testMethod(4)
	const char* args[] = { "prog", "-i", inputs[mono], "-c", 
	"+1  x ^ 18		 "
	"+1.45617x ^ 17 "
	"+1.3295x ^ 16	 "
	"+1.39688x ^ 15 "
	"+1.35196x ^ 14 "
	"+1.38584x ^ 13 "
	"+1.3581x ^ 12	 "
	"+1.38226x^11 "
	"+1.36012x ^ 10 "
	"+1.38134x ^ 9	 "
	"+1.36012x ^ 8	 "
	"+1.38226x ^ 7	 "
	"+1.3581x ^ 6	 "
	"+1.38584x ^ 5	 "
	"+1.35196x ^ 4	 "
	"+1.39688x ^ 3	 "
	"+1.3295x ^ 2	 "
	"+1.45617x ^ 1	 "
	"+1x ^ 0		 "
	"+1y ^ 18		 "
	"+1.44888y ^ 17 "
	"+1.31624y ^ 16 "
	"+1.37603y ^ 15 "
	"+1.32512y ^ 14 "
	"+1.35154y ^ 13 "
	"+1.31786y ^ 12 "
	"+1.3346y ^ 11	 "
	"+1.30665y ^ 10 "
	"+1.32041y ^ 9	 "
	"+1.29362y ^ 8	 "
	"+1.3081y ^ 7	 "
	"+1.27881y ^ 6	 "
	"+1.29841y ^ 5	 "
	"+1.26033y ^ 4	 "
	"+1.2957y ^ 3	 "
	"+1.22704y ^ 2	 "
	"+1.33722y ^ 1	 "
	"+0.913725y ^ 0 ",
	
	"-o", outputs[mono] };

	WaveFilter vaweFilter;
	return vaweFilter.runFromArgs(sizeof(args) / sizeof(char*), args);
}


int runTests()
{
	string line;

	try
	{
		create_directory("results");

		do
		{
			runTest(1);				//simple params
			runTest(2);				//coeffs file
			runTest(3);				//cli args
			runTest(4);				//coeffs string

			logTests();

			cout << "Press 'r' to repeat tests: ";
			getline(cin, line);
		} while (!line.empty() && line[0] == 'r');
	}
	catch (exception& ex)
	{
		cerr << ex.what() << endl;
		return -1;
	}

	return 0;
}


int main(const int argc, const char* argv[])
{
	if (argc == 2 && (strcmp(argv[1], "-t") == 0 || strcmp(argv[1], "--test") == 0))			//special mode; keep outside getopt
		return runTests();

	WaveFilter waveFilter;
	return waveFilter.runFromArgs(argc, argv);
}
