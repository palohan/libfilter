#include "misc.h"
#include "Convert.h"
#include "Utils.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <cstring>

using namespace std;

vector<uint8_t> Convert::floatTo(uint16_t bpsOut, uint16_t formatOut, const vector<float>& input)
{
	vector<uint8_t> output;
	size_t sampleIdx= 0;
	int32_t sampleVal;
	
	switch (bpsOut)
	{
	case 8:
		output.resize(input.size());

		for (uint8_t *outPtr =output.data(), *endPtr = (uint8_t *)vectorEndPtr(output); outPtr < endPtr; outPtr++, sampleIdx++)
			*outPtr = static_cast<uint8_t>(round(input.at(sampleIdx) * 0x80) + 0x80);					//8B WAV je uns char

		break;

	case 16:
		output.resize(input.size()*2);
		
		for (int16_t *outPtr = (int16_t*)output.data(), *endPtr = (int16_t*)vectorEndPtr(output); outPtr < endPtr; outPtr++, sampleIdx++)
			*outPtr = static_cast<int16_t>(round(input.at(sampleIdx) * 0x8000));

		break;

	case 24:
		output.resize(input.size()*3);
			
		for (int32_t *outPtr = (int32_t*)output.data(), *endPtr = (int32_t*)(vectorEndPtr(output) - 1); outPtr < endPtr; outPtr++, sampleIdx++)//bolo tu output._Mylast() - 1
		{
			sampleVal = static_cast<int32_t>(round(input.at(sampleIdx) * 0x800000)) & 0xFFFFFF;
			memcpy(outPtr, &sampleVal, 3);
			outPtr += 3;
		}
		break;

	case 32:
		if (formatOut == WavMeta::FLOAT)
			output.assign((uint8_t*)input.data(), (uint8_t*)vectorEndPtr(input));		//byte copy
		else
		{
			output.resize(input.size() * 4);
				
			for (int *outPtr = (int*)output.data(), *endPtr = (int*)vectorEndPtr(output); outPtr < endPtr; outPtr++, sampleIdx++)
				*outPtr = static_cast<int>(round(input.at(sampleIdx) * 0x80000000));
		}

		break;
	}

	return output;
}


vector<uint8_t> Convert::convert(uint16_t bpsIn, uint16_t formatIn, uint16_t bpsOut, uint16_t formatOut, const vector<uint8_t>& input)
{
	if (formatOut == WavMeta::FLOAT)
		bpsOut = 32;

	if (bpsIn == bpsOut && formatIn == formatOut)
		return input;

	//input uint8_t -> float -> output uint8_t
	return floatTo(bpsOut, formatOut, toFloat(bpsIn, formatIn, input, 1));
}


void Convert::convertWav(const path& pathIn, const path& pathOut, uint16_t bpsOut, uint16_t formatOut)
{
	return convertWav(WavMeta(pathIn), pathOut, bpsOut, formatOut);
}

void Convert::convertWav(const WavMeta& win, const path& pathOut, uint16_t bpsOut, uint16_t formatOut)
{
	auto& fmtChunk = win.m_wavHead._m._fmtChunk;

	Utils::saveBuff(pathOut, FORMATCHUNK(bpsOut, formatOut, fmtChunk.m_SampleRate, fmtChunk.m_Channels), convert(fmtChunk.m_BitsPerSample, fmtChunk.m_Format, bpsOut, formatOut, win.getByteVec()));
}
