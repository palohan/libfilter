#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include "WavMeta.h"
#include "misc.h"

using std::filesystem::path;
using namespace misc;

class WavMeta;
struct FORMATCHUNK;

namespace Convert
{
	std::vector<uint8_t> CLASS_API floatTo(uint16_t bpsOut, uint16_t formatOut, const std::vector<float>& input);
	std::vector<uint8_t> CLASS_API convert(uint16_t bpsIn, uint16_t formatIn, uint16_t bpsOut, uint16_t formatOut, const std::vector<uint8_t>& input);
	void CLASS_API convertWav(const path& pathIn, const path& pathOut, uint16_t bpsOut, uint16_t formatOut);
	void CLASS_API convertWav(const WavMeta& win, const path& pathOut, uint16_t bpsOut, uint16_t formatOut);


	template <typename T>										//zalezi len na bpsInp, nie na type, v akom su vzorky
	std::vector<float> toFloat(uint16_t bpsInp, uint16_t formInp, const std::vector<T>& input, bool bRawNumbers)
	{
		void* lastBYTE = (void*)vectorEndPtr(input),
			*firstByte = (void*)input.data();

		std::vector<float> floatSamples(vectorByteSize(input) / (bpsInp / 8));
		
		uint32_t maxSample;

		if (!bRawNumbers)
			maxSample = 1;						//len daj cislo do float vektora take ake je

		switch (formInp)
		{
			case WavMeta::PCM:
				switch (bpsInp)
				{
					case 8:
					{
						if (bRawNumbers)
							maxSample = 0x80;

						uint8_t //*end = (uint8_t*)lastBYTE,
							*start = (uint8_t*)firstByte;

						for (int i = 0; start + i < lastBYTE; ++i)			//char* sa zvysuje presne o 1 uint8_t
							floatSamples.at(i) = (float)(start[i] - 0x80) / maxSample;

						break;
					}

					case 16:
					{
						if (bRawNumbers)
							maxSample = 0x8000;

						short //*end = (short*)lastBYTE,
							*start = (short*)firstByte;

						for (int i = 0; start + i < lastBYTE; ++i)			//short* sa zvysuje presne o 2 Byty
							floatSamples.at(i) = (float)start[i] / maxSample;

						break;
					}

					case 24:
					{
						if (bRawNumbers)
							maxSample = 0x800000;

						uintptr_t it = reinterpret_cast<uintptr_t>(firstByte),
							end = reinterpret_cast<uintptr_t>(lastBYTE);

						int sample24;
						for (int i = 0; it < end; ++i, it += 3)
						{
							sample24 = *((int*)it) && 0xFFFFFF;					//take first 3 bytes from int
							if (sample24 > 0x7FFFFF)								//ak prevysuje 3B signed rozsah, je zaporne
								sample24 -= 0x1000000;

							floatSamples.at(i) = (float)sample24 / maxSample;
						}

						break;
					}

					case 32:
					{
						if (bRawNumbers)
							maxSample = 0x80000000;

						int //*end = (int*)lastBYTE,
							*start = (int*)firstByte;

						for (int i = 0; start + i < lastBYTE; ++i)				//int* sa zvysuje presne o 4 Byty
							floatSamples.at(i) = (float)start[i] / maxSample;

						break;
					}

					default:
						throw WavException(WavStatus::BAD_BIT_RATE);
				}
				break;											//KONIEC format case 1

			case WavMeta::FLOAT:
				if (bpsInp == 32)									//je to float format
					floatSamples.assign((float*)firstByte, (float*)lastBYTE);
				else
					throw WavException(WavStatus::BAD_BIT_RATE);
				break;											//vyskoci odtialto

			default:
				throw WavException(WavStatus::BAD_FORMAT);
		}

		return floatSamples;
	}
	
	
	template <typename T>										//zalezi len na bpsInp, nie na type, v akom su vzorky
	std::vector<float> toFloat(const FORMATCHUNK& fmtChunk, const std::vector<T>& input, bool bRawNumbers)
	{
		return toFloat(fmtChunk.m_BitsPerSample, fmtChunk.m_Format, input, bRawNumbers);
	}
	
}
