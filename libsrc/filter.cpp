
#include "filter.h"
#include "Utils.h"

using namespace std;
using namespace std::filesystem;


vector<float> Filter::filter(const WavMeta& wav, map<char,vector<sym<double>>>& symsMap)
{
	return filter(wav.getFloatVec(), wav.m_wavHead._m._fmtChunk.m_Channels, symsMap);
}

vector<float> Filter::filter(const vector<float>& input, const uint16_t channels, map<char, vector<sym<double>>>& symsMap)
{
	if (channels < 1)
		throw logic_error("need to have 1 or more channels!");

	if (input.size() % channels != 0)
		throw logic_error("input samples NOT split equally among channels!");

	int mapSize = symsMap.size();
	
	if (mapSize)
	{
		auto mapBegin = symsMap.begin();
		
		auto& vecFIR = mapBegin->second;				
		auto* vecIIR = mapSize > 1 ? &(++mapBegin)->second : nullptr;

		return filterCoeffs(input, channels, vecFIR, vecIIR);
	}
	else
		throw logic_error("symsMap is empty");
	
}


vector<float> Filter::filter(const vector<float>& input, double freqSample, uint16_t channels, double freqBase, bool bIIR = false, float slope = 0)
{
	if (channels < 1)
		throw logic_error("need to have 1 or more channels!");

	if (input.size() % channels != 0)
		throw logic_error("input samples NOT split equally among channels!");
		
	if (freqBase < m_lowestFreq || freqBase > freqSample / 2)
	{
		std::stringstream ss;
		ss << std::fixed << std::setprecision(2) << "Base filtering frequency not in range <LOWEST_FREQ, FreqSample / 2> = <"
			<< m_lowestFreq << "Hz, " << freqSample / 2 << "Hz>" << endl;

		throw logic_error(ss.str());
	}

	double zerosCount;

	if (modf(freqSample / freqBase, &zerosCount) == 0)						//ak pocetnul=cele cislo, klasicky hrebenovy filter
		return filterComb(input, channels, static_cast<uint16_t>(zerosCount), bIIR, slope);
	else
	{
		std::ofstream coefsFile;

		try
		{
			create_directory("logs");

			char buf[50];
			sprintf(buf, "logs/%s_%.2f_%d_%d.txt", bIIR ? "IIR" : "FIR", freqBase, (uint32_t)freqSample, channels);

			/*std::stringstream ss;
			ss << (bIIR ? "IIR" : "FIR") << "_" << fixed << setprecision(2) << freqBase << "_" << setprecision(0) << freqSample << "_" << channels << ".txt";*/

			path filePath(buf);

			coefsFile.open(filePath);

			if (coefsFile.is_open())
			{
				cout << "storing coefficients in " << filePath << endl;
			}
		}
		catch (exception& ex)
		{
			cerr << __func__ << ": " << ex.what() << endl;				//log but keep working
		}
		
		auto FIR = sym<double>::coeffs(freqBase, freqSample, 1);					//FIR koeficienty
		//vector<sym<complex<double>>> cFIR= sym<complex<double>>::coeffs(freqBase, freqSample, 1);
		

		ostream& outStream = coefsFile && coefsFile.is_open() ? coefsFile : cout;

		sym<double>::showVector(FIR, outStream);

		if (bIIR)
		{
			auto IIR = sym<double>::coeffs(freqBase, freqSample, slope);					//IIR koeficienty. slope je vyska polu
						
			sym<double>::showVector(IIR, outStream, 'y');

			return filterIIR(input, channels, FIR, IIR);
		}
		else
			return filterFIR(input, channels, FIR);
	}
}


#define indexInChannel(sampleNo) (channels * (sampleNo) + channelNo)

#define oneSampleComb(input, output) (output.at(indexInChannel(sampleNo)) = (input).at(indexInChannel(sampleNo)) + ((indexInChannel(sampleNo - zerosCount) < 0) ? 0 : (-(input).at(indexInChannel(sampleNo - zerosCount)) + bIIR * slope * (output).at(indexInChannel(sampleNo - zerosCount)))))

#define oneSampleFIR(input, output) (output)[indexInChannel(sampleNo)] += (indexInChannel(sampleNo - coefNo) >= 0 ? ((FIR).at(coefNo).m_val * (input).at(indexInChannel(sampleNo - coefNo))) : 0)
#define oneSampleIIR(input, output) oneSampleFIR(input, output) - ((coefNo + 1 < coeffsCount && indexInChannel(sampleNo - coefNo - 1) >= 0) ? (IIR).at(coefNo + 1).m_val * (output).at(indexInChannel(sampleNo-coefNo-1)) : 0)


vector<float> Filter::filterCoeffs(const vector<float>& input, const uint16_t channels, vector<sym<double>>& FIR, vector<sym<double>>* const pIIR/* = nullptr*/)
{	
	const long int coeffsCount = FIR.size();
		
	if (pIIR && coeffsCount != (long int)pIIR->size())
		throw logic_error("FIR and IIR vector sizes dont match");

	const auto inputSize = input.size();
	const long int samplesInChannel = inputSize / channels;
	vector<float> output(inputSize, 0);
	

	sym<double>::sort(FIR);


	//nijaky index nemoze byt UNSIGNED, lebo zaporny rozdiel vyhodnoti ako kladny a bude hladat prvok s obrovskym indexom	
	if (pIIR)
	{
		auto& IIR = *pIIR;
		
		sym<double>::sort(IIR);

		for (int channelNo = 0; channelNo < channels; channelNo++)
			for (int sampleNo = 0; sampleNo < samplesInChannel; sampleNo++)
				for (int coefNo = 0; coefNo < coeffsCount; coefNo++)
					oneSampleIIR(input, output);
	}
	else
	{
		for (int channelNo = 0; channelNo < channels; channelNo++)
			for (int sampleNo = 0; sampleNo < samplesInChannel; sampleNo++)
				for (int coefNo = 0; coefNo < coeffsCount; coefNo++)
					oneSampleFIR(input, output);
	}


	Utils::normalizeSelf(output);
	return output;
}

vector<float> Filter::filterFIR(const vector<float>& input, const uint16_t channels, vector<sym<double>>& FIR)
{
	return filterCoeffs(input, channels, FIR);
}

vector<float> Filter::filterIIR(const vector<float>& input, const uint16_t channels, vector<sym<double>>& FIR, vector<sym<double>>& IIR)
{
	return filterCoeffs(input, channels, FIR, &IIR);
}


vector<float> Filter::filterComb(const vector<float>& input, const uint16_t channels, const uint16_t zerosCount, bool bIIR, float slope)
{
	const auto inputSize = input.size();
	vector<float> output(inputSize, 0);

	intmax_t samplesInChannel = input.size() / channels;

	//nijaky index nemoze byt UNSIGNED, lebo zaporny rozdiel vyhodnoti ako kladny a bude hladat prvok s obrovskym indexom
	for (int channelNo = 0; channelNo < channels; channelNo++)
		for (int sampleNo = 0; sampleNo < samplesInChannel; sampleNo++)
			oneSampleComb(input, output);

	Utils::normalizeSelf(output);
	return output;
}


vector<float> Filter::filter(const WavMeta& wav, const double fRus, const bool bIIR, const float slope)
{
	return filter(wav.getFloatVec(), wav.m_wavHead._m._fmtChunk.m_SampleRate, wav.m_wavHead._m._fmtChunk.m_Channels, fRus, bIIR, slope);
}


bool Filter::writeFilteredCoeffs(const path& source, const path& target, vector<sym<double>>& FIR, vector<sym<double>>* IIR)
{
	try
	{
		WavMeta inFile(source);
		vector<float> filtered = filterCoeffs(inFile.getFloatVec(), inFile.m_wavHead._m._fmtChunk.m_Channels, FIR, IIR);
		Utils::writeWav(target, filtered, inFile.m_wavHead, true);
	}
	catch (...)
	{
		return false;
	}

	return true;
}


bool Filter::writeFilteredFIR(const path& source, const path& target, vector<sym<double>>& FIR)
{	
	return writeFilteredCoeffs(source, target, FIR);
}


bool Filter::writeFilteredIIR(const path& source, const path& target, vector<sym<double>>& FIR, vector<sym<double>>& IIR)
{
	return writeFilteredCoeffs(source, target, FIR, &IIR);
}


bool Filter::writeFiltered(const path& source, const path& target, float freq, bool bIIR, float slope)
{
	try
	{
		WavMeta inFile(source);
		vector<float> filtered = filter(inFile.getFloatVec(), inFile.m_wavHead._m._fmtChunk.m_SampleRate, inFile.m_wavHead._m._fmtChunk.m_Channels, freq, bIIR, slope);
		Utils::writeWav(target, filtered, inFile.m_wavHead, true);
	}
	catch (...)
	{
		return false;
	}

	return true;
}
