#pragma once

#include <vector>
#include <experimental/vector>
#include <algorithm>
#include <functional>
#include <complex>

#define _USE_MATH_DEFINES
#include <math.h>
#include <cassert>

#include <iostream>
#include <iomanip>

using std::vector;


//#include <utility>
//using namespace rel_ops;
//declares template functions for four relational operators (!=,>, <=, and >=), deriving their behavior from operator== (for !=) and from operator< (for >,<=, and >=)

template<typename T>
class CLASS_API sym
{
	public:
	T m_val = 0;
	char m_var = 0;
	short m_exp = 0;

	//sym operator +(const sym& other) const;					//operatory s othermi
	//sym operator *(const sym& other) const;
	//sym operator -(const sym& other) const;
	//sym operator /(const sym& other) const;

	//sym& operator +=(const sym& other);					//operatory s othermi
	//sym& operator *=(const sym& other);
	//sym& operator -=(const sym& other);
	//sym& operator /=(const sym& other);

	//bool operator ==(const sym& other) const;
	//bool operator !=(const sym& other) const;
	//bool operator <(const sym& other) const;
	//bool operator >(const sym& other) const;
	//
	//sym operator *(T u) const;									//operatory s cislami
	//sym operator /(T u) const;	
	//sym operator ^(short u) const;
	//sym operator -() const;


	sym(T val, char var, short exp) : m_val(val), m_var(tolower(var)), m_exp(exp)
	{
	}


	explicit sym(T val, char var) : sym(val, var, 1)
	{
	}


	sym(char var, short exp) : sym(1, var, exp)
	{
	}


	explicit sym(char var) : sym(1, var, 1)
	{
	}

	explicit sym(T val) : sym(val, 0, 0)
	{
	}

	sym() = default;
	~sym() = default;


	sym operator +(const sym& other) const
	{
		assert(m_var == other.m_var && m_exp == other.m_exp);			//rovnaju sa mocniny

		return sym(m_val + other.m_val, m_var, m_exp);
	}


	sym& operator +=(const sym& other)
	{
		return *this = *this + other;
	}


	sym operator *(const sym& other) const
	{
		assert(m_var == other.m_var);								//rovnaju sa mocniny

		return sym(m_val*other.m_val, m_var, m_exp + other.m_exp);
	}


	sym& operator *=(const sym& other)
	{
		return *this = *this * other;
	}


	sym operator -(const sym& other) const
	{
		assert(m_var == other.m_var&&m_exp == other.m_exp);			//rovnaju sa mocniny

		return sym(m_val - other.m_val, m_var, m_exp);
	}


	sym& operator -=(const sym& other)
	{
		return *this = *this - other;
	}


	sym operator /(const sym& other) const
	{
		assert(m_var == other.m_var);								//rovnaju sa mocniny

		return sym(m_val / other.m_val, m_var, m_exp - other.m_exp);
	}


	sym& operator /=(const sym& other)
	{
		return *this = *this / other;
	}


	bool operator ==(const sym& other) const
	{
		assert(m_var == other.m_var);

		return m_val == other.m_val && m_exp == other.m_exp && m_var == other.m_var;
	}


	bool operator !=(const sym& other) const
	{
		return !(*this == other);
	}


	bool operator <(const sym& other) const
	{
		assert(m_var == other.m_var);

		if (m_exp == other.m_exp)							//ak sa rovnaju mocniny
			return m_val < other.m_val;					//mensie je s mensim m_val

		return m_exp < other.m_exp;
	}


	bool operator >(const sym& other) const
	{
		return other < *this;
	}


	sym operator *(double u) const
	{
		return sym(m_val*u, m_var, m_exp);
	}


	sym operator /(double u) const
	{
		return sym(m_val / u, m_var, m_exp);
	}


	sym operator ^(short u) const
	{
		return sym(m_val, m_var, m_exp*u);				//umocnenie
	}


	sym operator -() const
	{
		return sym(-m_val, m_var, m_exp);
	}


	static vector<sym>& plusSelf(const vector<sym>& left, const vector<sym>& right)
	{
		left.insert(left.end(), right.begin(), right.end());			//pripojime druhy vektor

		return sort(left);
	}

	static vector<sym> plus(vector<sym> left, const vector<sym>& right)		//LAVY MUSI BYT KOPIA
	{
		return plusSelf(left, right);
	}


	static vector<sym>& minusSelf(const vector<sym>& left, const vector<sym>& right)
	{
		for (auto& rightVal : right)
			rightVal *= -1;

		left.insert(left.end(), right.begin(), right.end());			//pripojime druhy negovany vektor

		return sort(left);
	}

	static vector<sym> minus(vector<sym> left, const vector<sym>& right)		//LAVY MUSI BYT KOPIA
	{
		return minusSelf(left, right);
	}


	static vector<sym> multiply(const vector<sym>& left, const vector<sym>& right)
	{
		vector<sym> result(left.size() * right.size());
		size_t i = 0;

		for (auto& leftVal : left)
			for (auto& rightVal : right)
			result[i++] = (leftVal * rightVal);		//kazdy prvok sa nasobi s kazdym

		return sort(result);
	}


	static vector<sym>& sort(vector<sym>& vec)
	{
		const auto vecBegin = vec.begin(), vecEnd = vec.end();

		for (auto currentIt = vecBegin; currentIt != vecEnd; ++currentIt)
		{
			if (currentIt->m_var != -1)
			{
				for (auto otherIt = currentIt + 1; otherIt < vecEnd; ++otherIt)
				{
					if (currentIt->m_var == otherIt->m_var && currentIt->m_exp == otherIt->m_exp)
					{
						*currentIt += *otherIt;
						otherIt->m_var = -1;						//mark other for removal
					}
				}
			}
		}
		
		std::experimental::erase_if(vec, [](const sym &arg) { return arg.m_var == -1; });

		std::sort(vecBegin, vec.end(), std::greater<sym>());					//tu uz je vecEnd iny!!

		return vec;
	}


	static vector<sym> coeffs(double baseFreq, double sampleFreq, float slope)
	{
		int zerosCount = static_cast<int>(floor(sampleFreq / 2 / baseFreq));						//zisti pocet nul. tu musi byt /2 !!!

		const sym x2(1, 'x', 2);
		const sym x1(-2 * slope, 'x', 1);
		const sym x0(pow(slope, 2), 'x', 0);

		vector<sym> output(3), oneBrace(3);

		output[0] = x2;										//prva zatvorka. tu sa robi IIR / FIR
		output[1] = x1 * cos(2 * M_PI * baseFreq / sampleFreq);
		output[2] = x0;

		oneBrace[0] = x2;									//tieto NEMUSIM ratat
		oneBrace[2] = x0;

		for (int nthFreq = 2; nthFreq <= zerosCount; ++nthFreq)							//zacina od 2. harmonickej
		{
			oneBrace[1] = (x1 * cos(2 * M_PI * nthFreq * baseFreq / sampleFreq));

			output = multiply(output, oneBrace);
		}

		return output;
	}
	

	void show(FILE* stream = stdin, char varAlias = 0) const
	{
		fprintf(stream ? stream : stdin, "%+g%c^%d\n", m_val, varAlias ? varAlias : m_var, m_exp);
	}

	std::ostream& show(std::ostream& stream = std::cout, char varAlias = 0) const
	{		
		if (m_val)
		{
			stream << std::showpos << m_val;

			stream << (varAlias ? varAlias : m_var);
			
			stream << '^' << std::noshowpos << m_exp << std::endl;
		}

		return stream;
	}

	static void showVector(const vector<sym>& vec, std::ostream& stream = std::cout, char varAlias = 0)
	{
		for (auto& vecVal : vec)
			vecVal.show(stream, varAlias);
	}


	friend std::ostream& operator<< (std::ostream& stream, const sym& This)
	{
		return This.show(stream);
	}
	

	static void setVectorVars(vector<sym>& vec, char var)
	{
		for (auto& vecVal : vec)
			vecVal.m_var = var;
	}
};
