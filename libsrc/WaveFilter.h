#pragma once

#include <vector>
#include <filesystem>
#include "parser.h"
#include "filter.h"
#include "Utils.h"

using std::filesystem::path;

class WaveFilter
{
public:
	enum ParamType {
		FREQUENCY, COEF_STRING, COEF_FILE, none
	};

private:
	std::vector<float> m_filteredSamples;

	Parser m_parser;
	Parser::vectorMap m_vectorMap;
	ParamType m_freqType = ParamType::none;

	WavMeta m_wave;

	struct Params
	{
		bool m_IIR = false;
		double m_baseFreq, m_slope = -1;
		char* m_coeffs, *m_coeffsFile;
	} m_params;

	bool 
		m_paramsOK = false,
		m_filterOK = false;

public:
	CLASS_API void loadInputFile(const path& fileName);
	 
	CLASS_API bool setParamsGen(ParamType type);
	 
	CLASS_API bool setParams(bool iir, double baseFreq, double slope);
	 
	CLASS_API bool setCoeffsFile(const path & coeffsFileName);
	 
	CLASS_API bool setCoeffs(const std::string& strCoeffs);
	 
	CLASS_API void printHelp();
	 
	CLASS_API int usage(int status);
	 
	CLASS_API bool filter();
	 
	CLASS_API int saveOutput(const path & outputPath, bool saveAsFloat = false);
	 
	CLASS_API int runFromArgs(int argc, const char* argv[]);	
};
