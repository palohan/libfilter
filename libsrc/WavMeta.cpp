#include "WavMeta.h"
#include "Utils.h"
#include "Convert.h"

#include <cstring>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

const map<WavStatus, const char*> WavException::statusMsgMap =
{
	{WavStatus::WAV_FILE_OK ,	"File OK"					},
	{WavStatus::BAD_CHANNEL,	"Error: Unsupported channel"	},
	{WavStatus::BAD_FORMAT,		"Error: Unsupported format"		},
	{WavStatus::BAD_BIT_RATE,	"Error: Unsupported bit rate"	},
	{WavStatus::BAD_HEADER,		"Error: Malformed header"		},
	{WavStatus::FILE_NOT_OPEN,	"Error: Could not open the file"},
	{ WavStatus::FILE_EOF,		"Error: Premature End Of File" }
};

const char* WavException::getStatusInfo(WavStatus status)
{
	return statusMsgMap.at(status);
}

WavException::WavException(WavStatus status, const std::string& msg) : 
	logic_error(string(getStatusInfo(status)) + " " + msg),
	m_status(status)
{
}

WavStatus WavException::getStatus() const
{
	return m_status;
}


const map<uint16_t, const char*> WavMeta::vawFormats =
{
	{UNKNOWN,	"UNKNOWN"	},
	{PCM,		"PCM"		},
	{FLOAT,		"FLOAT"		},
	//{mulaw,		"mulaw"		},
	//{alaw,		"alaw"		},
};

const char* WavMeta::getVawFormat(uint16_t format)
{
	auto it = vawFormats.find(format);

	return it != vawFormats.end() ? it->second : vawFormats.at(WavFormat::UNKNOWN);
}

WavMeta::WavMeta(const path& filePath)
{
	loadFile(filePath);
}

WavMeta::WavMeta(const FORMATCHUNK & fmtChunk, const std::vector<uint8_t>& vecSamples)
{
	try
	{
		fmtChunk.validate();

		m_wavHead = {};

		m_wavHead._m._fmtChunk = fmtChunk;

		m_byteVec = vecSamples;
		m_floatVec = Convert::toFloat(fmtChunk, m_byteVec, false);

		m_wavHead._m._fmtMeta.m_size = sizeof fmtChunk;
		m_wavHead._m._dataMeta.m_size = vectorByteSize(m_byteVec);
		m_wavHead._m._riffMeta.m_size = m_wavHead._m._dataMeta.m_size + WavHeader::RiffSizeWithoutData;

		calculateTotals();
		m_fileSize = sizeof(m_wavHead) + m_wavHead._m._dataMeta.m_size;		//TODO: move to calculateTotals?

		m_wavStatus = WavStatus::WAV_FILE_OK;
	}
	catch (WavException& ex)
	{
		m_wavStatus = ex.getStatus();
		throw;
	}
}


bool findChunk(ifstream& fin, ChunkMeta& chunkMeta, const char* chunkID)
{
	while (fin.read((char*)&chunkMeta, sizeof(chunkMeta)))
		if (strncmp(chunkMeta.m_ID.data(), chunkID, sizeof(chunkMeta.m_ID)) == 0)
			return true;
		else
			fin.seekg(chunkMeta.m_size, fin.cur);			//move to next chunk

	return false;
}


void WavMeta::loadFile(const path& filePath)
{
	m_wavHead = {};

	m_wavStatus = WavStatus::WAV_FILE_OK;
	

	auto& riffChunk = m_wavHead._m._riffChunk;
	auto& fmtChunk = m_wavHead._m._fmtChunk;

	ifstream fin(filePath, ios::binary);

	try
	{
		if (!fin.is_open())
			throw WavException(WavStatus::FILE_NOT_OPEN, filePath.string());

		m_fileSize = file_size(filePath);

		fin.read((char*)&m_wavHead._m._riffMeta, sizeof(m_wavHead._m._riffMeta));
		fin.read((char*)&riffChunk, sizeof(riffChunk));

		if (!m_wavHead.isRiffValid() || !fin)
			throw WavException(WavStatus::BAD_HEADER, "RIFF not found or invalid");

		auto pos = fin.tellg();

		if (!findChunk(fin, m_wavHead._m._fmtMeta, "fmt "))
			throw WavException(WavStatus::BAD_HEADER, "no fmt chunk");

		if (!fin.read((char*)&fmtChunk, sizeof(fmtChunk)))
			throw WavException(WavStatus::BAD_HEADER, "cant read fmt chunk");

		cout << fmtChunk.print();

		//auto fmtChunkEndPos = fin.tellg();

		fmtChunk.validate();

		if (!findChunk(fin, m_wavHead._m._dataMeta, "data"))
			throw WavException(WavStatus::BAD_HEADER, "no data chunk");

		pos = fin.tellg();

		calculateTotals();

		m_byteVec.resize(m_wavHead._m._dataMeta.m_size);								//vyhradime miesto vo vektore pre vzorky
		if (!fin.read((char*)m_byteVec.data(), m_wavHead._m._dataMeta.m_size))					//citam vzorky pre OLD TRACK do char vektora
			throw WavException(WavStatus::FILE_EOF);

		m_floatVec = Convert::toFloat(fmtChunk, m_byteVec, false);
	}
	catch (WavException& ex)
	{
		m_wavStatus = ex.getStatus();
		throw;
	}

	//printInfo();
}

size_t WavMeta::getTotalSamples() const
{
	return m_totalSamples;
}

size_t WavMeta::getTotalSamplesInChannel() const
{
	return m_totalSamplesPerChannel;
}

size_t WavMeta::getTrackTime() const
{
	return m_trackTime;
}

uintmax_t WavMeta::getFileSize() const
{
	return m_fileSize;
}

const std::vector<float>& WavMeta::getFloatVec() const
{
	return m_floatVec;
}

const std::vector<uint8_t>& WavMeta::getByteVec() const
{
	return m_byteVec;
}

void WavMeta::exitOnError(WavStatus status, const string& msg)
{
	m_wavStatus = status;	
	throw WavException(status, msg);
}

void WavMeta::calculateTotals()
{
	//not in the WavHeader

	const auto BYTEsPerSample = m_wavHead._m._fmtChunk.m_BitsPerSample / 8;

	m_totalSamples = m_wavHead._m._dataMeta.m_size / BYTEsPerSample;
	m_totalSamplesPerChannel = m_totalSamples / m_wavHead._m._fmtChunk.m_Channels;
	m_trackTime = m_totalSamplesPerChannel * 1000 / m_wavHead._m._fmtChunk.m_SampleRate;
}


FORMATCHUNK::FORMATCHUNK(uint16_t bps, uint16_t format, uint32_t fs, uint16_t channels) :
	m_Format(format),
	m_Channels(channels),
	m_SampleRate(fs),
	m_BitsPerSample(bps)
{
	reCalculate();
}

void FORMATCHUNK::reCalculate(bool toFloat)
{
	if (toFloat)
	{
		m_Format = WavMeta::FLOAT;
		m_BitsPerSample = 32;
	}

	m_BlockAlign = m_BitsPerSample / 8 * m_Channels;
	m_ByteRate = m_BlockAlign * m_SampleRate;
}

bool FORMATCHUNK::isFloat() const
{
	return m_BitsPerSample == 32 && m_Format == WavMeta::FLOAT;
}

void FORMATCHUNK::validate() const
{
	if (m_Channels < Channel::MONO || m_Channels > Channel::LAST)
		throw WavException(WavStatus::BAD_CHANNEL, to_string(m_Channels));
}

std::string FORMATCHUNK::getFormatInfo() const
{
	return std::string(WavMeta::getVawFormat(m_Format)) + to_string(m_BitsPerSample);
}


#define varInfo(var) "\t"#var"[" + std::to_string(sizeof(m_##var)) + "B]:" + std::to_string(m_##var) + "\n"

string FORMATCHUNK::print() const
{
	string info = "FORMATCHUNK:\n"
		varInfo(Format) +
		varInfo(Channels) +
		varInfo(SampleRate) +
		varInfo(ByteRate) +
		varInfo(BlockAlign) +
		varInfo(BitsPerSample);

	return info;
}

bool WavHeader::isRiffValid() const
{
	return strncmp(_m._riffMeta.m_ID.data(), "RIFF", 4) == 0 && strncmp(_m._riffChunk.WAVE.data(), "WAVE", 4) == 0;
}