#pragma once

#include <vector>

#define __FUNC_NAME_COLON__   std::string(__func__) + ": "

namespace misc
{
	template <typename T> const T* vectorEndPtr(const std::vector<T>& vec)
	{
		return vec.data() + vec.size();
	}

	template <typename T> auto vectorByteSize(const std::vector<T>& vec)
	{
		return vec.size() * sizeof(typename std::vector<T>::value_type);
	}
}
