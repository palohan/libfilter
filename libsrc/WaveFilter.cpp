// testFilter.cpp : Defines the entry point for the console application.
//



#include "parser.h"
#include "WaveFilter.h"
#include "filter.h"
#include "Utils.h"

#include <string>
#include <iostream>
#include <fstream>

#ifdef _WIN32
#include "getopt.h"
#else
#include <unistd.h>
#include <getopt.h>
#endif


using namespace std;
using namespace std::filesystem;


void WaveFilter::loadInputFile(const path& fileName)
{
	m_wave.loadFile(fileName);
}

bool WaveFilter::setParamsGen(ParamType type)
{
	switch (type)
	{
		case ParamType::FREQUENCY:
			return setParams(m_params.m_IIR, m_params.m_baseFreq, m_params.m_slope);

		case ParamType::COEF_FILE:
			return setCoeffsFile(m_params.m_coeffsFile);

		case ParamType::COEF_STRING:
			return setCoeffs(m_params.m_coeffs);

		default:
			break;
	}

	return false;
}


bool WaveFilter::setParams(bool iir, double baseFreq, double slope)
{
	if (slope <= 0 || slope >= 1)
	{
		cerr << "IIR slope not in range (0, 1)" << endl;
		return m_paramsOK = false;
	}

	const auto freqMax = m_wave.m_wavHead._m._fmtChunk.m_SampleRate / 2;

	if (baseFreq < Filter::m_lowestFreq || baseFreq > freqMax)
	{
		cerr << std::fixed << std::setprecision(2) << "Base filtering frequency not in range <LOWEST_FREQ, FreqSample / 2> = <"
			<< Filter::m_lowestFreq << "Hz, " << freqMax << "Hz>" << endl;

		return m_paramsOK = false;
	}

	
	m_params.m_IIR = iir;
	m_params.m_baseFreq = baseFreq;
	m_params.m_slope = slope;
	m_freqType = ParamType::FREQUENCY;

	return m_paramsOK = true;
}

bool WaveFilter::setCoeffsFile(const path& coeffsPath)
{
	m_paramsOK = false;
	
	std::ifstream coeffsFile(coeffsPath);

	if (!coeffsFile.is_open())
	{
		cerr << "coeffs file does not exist" << endl;
		return m_paramsOK = false;
	}

	try
	{
		m_vectorMap = m_parser.getVectorsMap(coeffsFile);
		m_freqType = ParamType::COEF_FILE;

		m_paramsOK = true;
	}
	catch (std::exception& ex)
	{
		cerr << __func__ << ": " << ex.what() << endl;
		throw;
	}

	return m_paramsOK;
}


bool WaveFilter::setCoeffs(const string& strCoeffs)
{
	m_paramsOK = false;

	auto stream = istringstream(strCoeffs);

	try
	{
		m_vectorMap = m_parser.getVectorsMap(stream);

		m_freqType = ParamType::COEF_STRING;
		m_paramsOK = true;
	}
	catch (std::exception& ex)
	{
		cerr << __func__ << ": " << ex.what() << endl;
		throw;
	}

	return m_paramsOK;
}


void WaveFilter::printHelp()
{
	cerr
		<< "usage:" << endl
		<< "clifilter -i <input file> (-f <frequency> [-I <slope>] | -c <coeffs> | -C <coeffs path>) [-n] -o <output file>" << endl
		<< "run tests:" << endl
		<< "clifilter - t" << endl
		<< endl
		<< "Options:" << endl
		<< "\t" "-i, --input"		 "\t\t" << "path to input WAV" << endl
		<< "\t" "-f, --frequency"	 "\t\t" << "fundamental frequency of the unwanted signal <100, FS/2>" << endl
		<< "\t" "-I, --IIR"			 "\t\t" << "sets IIR mode and its slope (0, 1). Otherwise, the mode is FIR " << endl
		<< "\t" "-c, --coeffs"		 "\t\t" << "quoted string of filtering coefficients - a polynomial in form of \"1x^18 + ... + 3x^0 - 5y^18 - ... -2y^0\"" << endl
		<< "\t" "-C, --CoeffsPath"   "\t" << "path to the file containing filtering coefficients" << endl
		<< "\t" "-n, --nativeFormat" "\t" << "saves the output WAV in input's format; otherwise in FLOAT32" << endl
		<< "\t" "-o, --output"		 "\t\t" << "path to output WAV " << endl
		<< "\t" "-t, --test"		 "\t\t" << "runs predefined tests. Input WAVs are loaded from ./samples/, logs and results are written to ./logs/ and ./results/ respectively" << endl
		;
}

int WaveFilter::usage(int status)
{
	printHelp();
	return status;
}


bool WaveFilter::filter()
{
	if (!m_paramsOK)
	{
		throw logic_error(__FUNC_NAME_COLON__  "Invalid filter params");
	}

	try
	{
		switch (m_freqType)
		{
			case ParamType::FREQUENCY:
				m_filteredSamples = Filter::filter(m_wave, m_params.m_baseFreq, m_params.m_IIR, m_params.m_slope);
				break;

			case ParamType::COEF_STRING:
			case ParamType::COEF_FILE:
				m_filteredSamples = ::Filter::filter(m_wave, m_vectorMap);
				break;

			default:
				throw logic_error(__FUNC_NAME_COLON__  "Invalid filter params");
		}
	}
	catch (std::exception& ex)
	{
		cerr << __FUNC_NAME_COLON__ << ex.what() << endl;
		throw;
	}

	return m_filterOK = true;
}


int WaveFilter::saveOutput(const path& outputPath, bool saveAsFloat)
{
	if (!m_filterOK)
	{
		throw logic_error(__FUNC_NAME_COLON__  "Cannot save. Please call filter() first.");
	}

	auto status = Utils::writeWav(outputPath, m_filteredSamples, m_wave.m_wavHead, saveAsFloat);

	if (status == -1)
		cerr << "Failed writing result file!" << endl;
	else
		cout << "result in: " << outputPath << endl;

	return status;
}


int WaveFilter::runFromArgs(int argc, const char* argv[])
{
	static struct option long_options[] =
	{
		// These options set a flag.
		{ "nativeFormat",	no_argument,		0, 'n' },

		// These options don’t set a flag. We distinguish them by their indices.
		{ "input",		required_argument,		0, 'i' },
		{ "output",		required_argument,		0, 'o' },
		{ "frequency",  required_argument,		0, 'f' },
		{ "IIR",		required_argument,		0, 'I' },
		{ "coeffs",		required_argument,		0, 'c' },
		{ "CoeffsFile", required_argument,		0, 'C' },
		{ 0, 0, 0, 0 }
	};


	/* getopt_long stores the option index here. */
	int option_index = 0, optChar;

	char *inFile = nullptr,
		*outFile = nullptr;

	bool saveFloat = true;
	bool modesToTry[ParamType::none] = { 0 };
		
	optind = 1;					//reset getopt
	

	while (1)
	{
		optChar = getopt_long(argc, const_cast<char**>(argv), "i:o:nf:c:C:I:h", long_options, &option_index);

		/* Detect the end of the options. */
		if (optChar == -1)
			break;

		switch (optChar)
		{
			case 0:
				/* If this option set a flag, do nothing else now. */
				if (long_options[option_index].flag != 0)
					break;

				printf("option %s", long_options[option_index].name);

				if (optarg)
					printf(" with arg %s", optarg);

				printf("\n");
				break;

			case 'i':
				inFile = optarg;
				break;

			case 'o':
				outFile = optarg;
				break;

			case 'I':				
				if (!Utils::strToDouble(optarg, m_params.m_slope))
					cerr << "IIR slope is not a double" << endl;
				else
					m_params.m_IIR = true;
				break;

			case 'f':
				if (!Utils::strToDouble(optarg, m_params.m_baseFreq))
					cerr << "frequency is not a double" << endl;
				else
					modesToTry[ParamType::FREQUENCY] = true;
				break;

			case 'c':
				modesToTry[ParamType::COEF_STRING] = true;
				m_params.m_coeffs = optarg;
				break;

			case 'C':
				modesToTry[ParamType::COEF_FILE] = true;
				m_params.m_coeffsFile = optarg;
				break;

			case 'n':
				saveFloat = false;
				break;

			case '?':
				/* getopt_long already printed an error message. */
				break;

			default:
				return usage(10);
		}
	}


	if (!inFile || !outFile)
	{
		cerr << "Error: Input or output file not specified" << endl;
		return usage(5);
	}


	try
	{
		loadInputFile(inFile);

		for (size_t type = 0; type < ParamType::none; ++type)
		{
			try
			{
				if (modesToTry[type])
					if (setParamsGen(static_cast<ParamType>(type)))
						break;
			}
			catch (exception& ex)			//supress
			{				
			}
		}
		
		//bool res = 
		filter();

		return saveOutput(outFile, saveFloat);
	}
	catch (WavException& ex)
	{
		cerr << ex.what() << endl;
		return static_cast<int>(ex.getStatus());
	}
	catch (exception& ex)
	{
		cerr << ex.what() << endl;
		return -1;
	}
	catch (...)
	{
		cerr << "Unknown exception" << endl;
		return -1;
	}
}
