#pragma once
#include <vector>
#include <map>

#include <filesystem>

#include "WavMeta.h"

using std::vector;
using std::map;
using std::filesystem::path;

#include "sym.h"

class CLASS_API Filter
{
public:
	static constexpr double m_lowestFreq = 100;

	static vector<float> filter(const WavMeta& wav, map<char, vector<sym<double>>>& symsMap);
	static vector<float> filter(const vector<float>& input, const uint16_t channels, map<char, vector<sym<double>>>& symsMap);		//ENTRY

	static vector<float> filter(const WavMeta& wav, double fRus, bool bIIR, float slope);
	static vector<float> filter(const vector<float>& input, double fSample, uint16_t kan, double fRus, bool bIIR, float slope);		//ENTRY
	 
	 	 
	static bool writeFilteredFIR(const path& source, const path& target, vector<sym<double>>& FIR);
	static bool writeFilteredIIR(const path& source, const path& target, vector<sym<double>>& FIR, vector<sym<double>>& IIR);
	static bool writeFiltered(const path& source, const path& target, float FREQUENCY, bool bIIR = false, float slope = 0);

private:
	static vector<float> filterFIR(const vector<float>& input, uint16_t channels, vector<sym<double>>& FIR);						//FIR verzia
	static vector<float> filterIIR(const vector<float>& input, uint16_t channels, vector<sym<double>>& FIR, vector<sym<double>>& IIR);		//IIR verzia
	static vector<float> filterCoeffs(const vector<float>& input, uint16_t channels, vector<sym<double>>& FIR, vector<sym<double>>* IIR = nullptr);

	static vector<float> filterComb(const vector<float>& input, uint16_t channels, uint16_t cela, bool bIIR, float slope);

	static bool writeFilteredCoeffs(const path& source, const path& target, vector<sym<double>>& FIR, vector<sym<double>>* IIR = nullptr);
};
