#pragma once

#include "misc.h"
#include "WavMeta.h"
#include <fstream>
#include <filesystem>


using std::filesystem::path;

namespace Utils
{
	bool CLASS_API strToDouble(const char* str, double& value);

	void CLASS_API normalizeSelf(std::vector<float>& input, float maxSample = 1);
	std::vector<float> CLASS_API normalize(std::vector<float> input, float maxSample = 1);

	using std::ofstream;

	int CLASS_API writeWav(const path& outputPath, const std::vector<float>& filtered, const WavHeader& m_wavHead, bool saveFloat);


	template <typename typ> bool saveBuff(const path& saveTo, const FORMATCHUNK& formatChunk, const std::vector<typ>& samples)
	{
		if (!samples.size())
			return false;

		ofstream fout(saveTo, ofstream::binary);

		if (!fout)
			return false;

		WavMeta w;

		w.m_wavHead._m._fmtMeta.m_size = sizeof(formatChunk);
		w.m_wavHead._m._fmtChunk = formatChunk;
		w.m_wavHead._m._dataMeta.m_size = vectorByteSize(samples);
		w.m_wavHead._m._riffMeta.m_size = w.m_wavHead._m._dataMeta.m_size + WavHeader::RiffSizeWithoutData;

		
		fout.write((const char*)&w.m_wavHead, sizeof(w.m_wavHead));
		fout.write((const char*)samples.data(), w.m_wavHead._m._dataMeta.m_size);
		//auto bytesWritten = fout.tellp();

		return true;
	}


	std::vector<float> CLASS_API sineWave(float sampleCap, float fBase, float fSample, uint32_t size, int maxHarmonics = -1);

	std::vector<uint8_t> CLASS_API addSineWave(float sampleCap, float fBase, std::vector<float> input, const FORMATCHUNK& formatChunk, int maxHarmonics = -1);
	std::vector<uint8_t> CLASS_API addSineWave(float sampleCap, float fBase, const std::vector<uint8_t>& input, const FORMATCHUNK& formatChunk, int maxHarmonics = -1);
}