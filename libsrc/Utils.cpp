
#include <cstring>
#include <limits>
#include <fstream>
#include <algorithm>

#include "Convert.h"
#include "Utils.h"
#include "sym.h"

using namespace std;

bool CLASS_API Utils::strToDouble(const char * str, double & value)
{
	char *endptr = nullptr;

	value = strtod(str, &endptr);

	return *endptr == 0;
}

void Utils::normalizeSelf(vector<float>& input, float sampleCap/* = 1*/)
{
	if (!input.size())
		return;
		
	if (sampleCap <= 0 || sampleCap > 1)
		sampleCap = 1;

	auto minmax = std::minmax_element(input.begin(), input.end());

	auto maxSample = max(abs(*minmax.first), abs(*minmax.second));
		
	if (!isnormal(maxSample))
		throw logic_error("samples are corrupted");
	
	for (auto& inputVal : input)
	{
		inputVal /= maxSample;				//vzorky sa podelia max vzorkou
		inputVal *= sampleCap;						//a vynasobia stropom
	}
}


vector<float> Utils::normalize(vector<float> input, float sampleCap/* = 1*/)
{
	normalizeSelf(input, sampleCap);

	return input;
}


int Utils::writeWav(const path& outputPath, const vector<float>& filtered, const WavHeader& m_wavHead, bool saveFloat)
{
	ofstream fout(outputPath, ofstream::binary);

	if (!fout)															//neotvoril subor
		return -1;


	const void* dataPtr = nullptr;
	vector<uint8_t> byteVector;
	bool changeType = false;
	std::streampos pos;

	WavHeader head2 = m_wavHead;											//head2 patri novemu suboru
	auto& fmtChunk = head2._m._fmtChunk;

	head2._m._fmtMeta.m_size = sizeof(fmtChunk);								//toto opravim
		
	if (!saveFloat)													//chce povodny typ
	{
		if (fmtChunk.isFloat())				//povodny typ 32bitovy je float, nic nemen
			saveFloat = true;
		else
		{
			byteVector = Convert::floatTo(fmtChunk.m_BitsPerSample, fmtChunk.m_Format, filtered);
			dataPtr = byteVector.data();
		}

		if (!dataPtr)
		{
			saveFloat = true;								//nepodporovany bitrate ulozime float vzorky
			changeType = true;								//bol nuteny zmenit typ
		}
	}

	if (saveFloat)						//nechcel povodny typ, alebo chcel ulozit zly bitrate, alebo povodny typ bol float
	{
		fmtChunk.reCalculate(true);
			
		head2._m._dataMeta.m_size = vectorByteSize(filtered);			//vzorky*kanaly v B v celom tracku
		dataPtr = filtered.data();										//ulozime float vzorky
	}
																
	head2._m._riffMeta.m_size = head2._m._dataMeta.m_size + WavHeader::RiffSizeWithoutData;
		
	fout.write((const char*)&head2, sizeof(head2)); 				//zapiseme celu hlavu		
	pos = fout.tellp();
	fout.write((const char*)dataPtr, head2._m._dataMeta.m_size); 	//hned za "dataPtr" idu vzorky
	pos = fout.tellp();
		
	if (!fout)
		return -1;
		
	//nedalo sa ulozit v nativeType, ulozil vo floate. inak v povodnom type
	return changeType ? 1 : 0;
}


vector<float> Utils::sineWave(float sampleCap, float fBase, float fSample, uint32_t size, int maxHarmonics/* = -1*/)
{
	vector<float> output(size, 0);

	int harmonics = static_cast<int>(fSample / fBase / 2);

	if (maxHarmonics > 0 && harmonics > maxHarmonics)
		harmonics = maxHarmonics;
	
	const auto tmp = 2 * M_PI * fBase / fSample;

	for (uint32_t i = 0; i < size; i++)							//vzorky
		for (int j = 1; j <= harmonics; j++)						//harmonicke
			output.at(i) += (float)sin(tmp * i * j);

	normalizeSelf(output, sampleCap);

	return output;
}


vector<uint8_t> Utils::addSineWave(float sampleCap, float fBase, const vector<uint8_t>& input, const FORMATCHUNK& formatChunk, int maxHarmonics/* = -1*/)
{
	return addSineWave(sampleCap, fBase, Convert::toFloat(formatChunk, input, true), formatChunk, maxHarmonics);
}


vector<uint8_t> Utils::addSineWave(float sampleCap, float fBase, vector<float> input, const FORMATCHUNK& formatChunk, int maxHarmonics/* = -1*/)
{
	const auto inputSize = input.size();
	
	vector<float> sine = sineWave(sampleCap, fBase, formatChunk.m_SampleRate, inputSize, maxHarmonics);
	
	for (uint32_t i = 0; i < inputSize; i++)
		input.at(i) += sine.at(i);

	normalizeSelf(input);

	return Convert::floatTo(formatChunk.m_BitsPerSample, formatChunk.m_Format, input);
}
