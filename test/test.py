import os
import pathlib
import subprocess
import unittest
from typing import Dict

from matplotlib.ticker import FormatStrFormatter
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave


#os.chdir(os.getcwd())


def show_spectrum(filepath, base_freq):
    rate, data = wave.read(filepath)
    spectre = np.fft.fft(data)
    freq = np.fft.fftfreq(data.size, 1 / rate)
    mask = freq > 0

    fig, ax1 = plt.subplots(1, 1, figsize=(10, 6))

    ax1.plot(freq[mask], np.abs(spectre[mask]))

    ax1.set_ylim(bottom=0)
    ax1.set_xlim(left=0)
    ax1.set_xlabel('Frequency [Hz]')

    ax2 = ax1.twiny()  # Remark: twiny will create a new axes where the y-axis is shared with ax1, but the x-axis is
    # independent - important!
    ax2.set_xlim(ax1.get_xlim())  # ensure the independent x-axes now span the same range

    bad_freqs = np.arange(base_freq, rate / 2, base_freq)

    for freq in bad_freqs:
        plt.axvline(freq, ls='--', color='red')

    ax2.set_xticks(bad_freqs)
    ax2.xaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax2.tick_params(axis='x', which='both', colors='red')

    fig.canvas.set_window_title(filepath + ' - spectrum')
    fig.tight_layout()

    plt.show()


class MyTests(unittest.TestCase):
    """filter -i <input_path> (-f <frequency> [-I <slope>] | --c <coeffs> | -C <coeffs_path>) [-n] -o <output path>"""

    inputs = {'mono': "samples/rjs01.wav", 'stereo': "samples/rjsStereo.wav"}
    program = './filter'

    def setUp(self):
        if not os.path.isdir("samples"):
            raise FileNotFoundError

        pathlib.Path('results').mkdir(exist_ok=True)

    @staticmethod
    def GetOutputNames(test_id):
        outputs: Dict[str, str] = {'mono': f"results/mono{test_id}.wav", 'stereo': f"results/stereo{test_id}.wav"}
        return outputs

    def run_test(self, test_id, channels, args):
        outputname = self.GetOutputNames(test_id)[channels]

        popenargs = [self.program, '-i', self.inputs[channels], '-o', outputname]
        popenargs.extend(map(str, args))

        print('executing:')
        print(*popenargs, sep=' ')

        process = subprocess.run(popenargs)
        process.check_returncode()

        show_spectrum(outputname, 431.37)

    def test1_params(self):
        self.run_test(1, 'mono', ['-f', 431.37, '-I', 0.995])

    def test2_coefFile(self):
        self.run_test(2, 'mono', ['-C', "logs/IIR_431.37_8000_1.txt"])

    def test3_coefString(self):
        coeffs = """
            +1  x ^ 18	
            +1.45617x ^ 17 
            +1.3295x ^ 16
            +1.39688x ^ 15 
            +1.35196x ^ 14 
            +1.38584x ^ 13 
            +1.3581x ^ 12
            +1.38226x^11
            +1.36012x ^ 10 
            +1.38134x ^ 9
            +1.36012x ^ 8
            +1.38226x ^ 7
            +1.3581x ^ 6
            +1.38584x ^ 5
            +1.35196x ^ 4
            +1.39688x ^ 3
            +1.3295x ^ 2
            +1.45617x ^ 1
            +1x ^ 0
            +1y ^ 18	
            +1.44888y ^ 17 
            +1.31624y ^ 16 
            +1.37603y ^ 15 
            +1.32512y ^ 14 
            +1.35154y ^ 13 
            +1.31786y ^ 12 
            +1.3346y ^ 11
            +1.30665y ^ 10 
            +1.32041y ^ 9
            +1.29362y ^ 8
            +1.3081y ^ 7
            +1.27881y ^ 6
            +1.29841y ^ 5
            +1.26033y ^ 4
            +1.2957y ^ 3
            +1.22704y ^ 2
            +1.33722y ^ 1
            +0.913725y ^ 0 
        """
        self.run_test(3, 'mono', ['-c', coeffs])


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTests)

    runner = unittest.TextTestRunner(verbosity=3, failfast=True)
    runner.run(suite)
