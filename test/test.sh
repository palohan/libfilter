#!/bin/bash

DIR="${0%/*}"

source $DIR/env/bin/activate
python3 $DIR/test.py
deactivate