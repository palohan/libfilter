# Introduction #

This is a command line impulse response digital WAV filter. Specify the input WAV, filter out the defective signal and save the output track.

# Background

I took a DSP & coding class back in summer semester of 2012. To complete an assignment in both, I took up a task of creating a comb fitler in C++. This project helped me overcome my big initial fear of coding (which I think puts off a lot of people) - back then I knew only a little bit of C. I was really green back then - didn't know about dynamic memory, const refs and even debugging existed! I couldn't count the number of hours I spent on it - every little thing I learned I had to try here. Every line has been visited & revisited many times over the years - **the project reflects my coding skill (as of May 2017) pretty well.** 

To conclude: Had I given up on this, I would not be coding today.


# Repo description

Written in starnard C++17 (uses std::experimental::filesystem).

This repo is included as a subtree within my other repo **wavfilter**, which uses it as a DLL for a GIU and CLI programs.

The repo has these important folders:

- libsrc - the "library" part - contains the WAV processing code, without the main() function
- src - contains the main() and test functions - makes an executable together with libsrc/ files
- samples - mono & stereo tracks, with defective signal with FF=431.37Hz

# Building

Just cmake - EASY! Visual studio 2017 comes with built in cmake support; otherwise, you would do:


```
#!

cd clishop
mkdir build
cd build
cmake ..
make
```



# Terminology
- FF - fundamental frequency
- FS - sampling frequency
- BPS - bits per sample
- Fmax - maximum possible frequency under current FS = FS/2
- Comb filter - filters out all the harmonics of a periodic signal with given FF
- FIR - finite impulse response - each output sample is a product of filter's coefficients and input samples preceding it
- IIR - infinite impulse response - each output sample is a product of filter's coefficients and input AND output samples preceding it
- WAV formats:
	- PCM - samples stored in integers, distinguished by the BPS:
		- 8b unsigned
		- 16b, 24b, 32b signed
	- FLOAT - 4B float number
	
	
## Program usage
```
#!

usage:
clifilter -i <input path> (-f <frequency> [-I <slope>] | --c <coeffs> | -C <coeffs path>) [-n] -o <output path>
run tests:
clifilter -t

Options:
        -i, --input             path to input WAV
        -f, --frequency         fundamental frequency of the unwanted signal <100, FS/2>
        -I, --IIR               sets IIR mode and its slope (0, 1). Otherwise, the mode is FIR 
        -c, --coeffs            quoted string of filtering coefficients - a polynomial in form of "1x^18 + ... + 3x^0 - 5y^18 - ... -2y^0"
        -C, --CoeffsPath        path to the file containing filtering coefficients
        -n, --nativeFormat      saves the output WAV in input's format; otherwise in FLOAT32
        -o, --output            path to output WAV 
		-t, --test				runs predefined tests: input WAVs are loaded from ./samples/, logs and results are written to ./logs/ and ./results/ respectively
```

### License ###

You are free to use this in whatever non-money-making way. I hold NO responsibility for any damages.

### The Future ###

If I see some C++ hotness, be sure I'll test it here. I tried my best, but if you find some bugs, please report them.

### Contact ###

I am open to any suggestions. I can't promise speedy replies though.

# Have fun! #